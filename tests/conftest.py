"""Test setup."""
from pathlib import Path
from unittest.mock import patch
from typing import IO, Iterator, Callable
import pytest  # type: ignore
from imagestore.config import Config
from flask import Flask


TEST_ROOT = Path(__file__).parent.resolve()
PROJECT_ROOT = TEST_ROOT.parent


@pytest.fixture
def uploads_dir(tmpdir) -> Path:
    """Get test uploads directory."""
    path = Path(str(tmpdir)) / 'uploads'
    path.mkdir()
    return path


@pytest.fixture
def config(uploads_dir: Path):
    """
    Patch the config with test settings.

    This needs to happen before the app is imported.
    """
    with patch.multiple(
        Config,
        SERVER_NAME='127.0.0.1:9000',
        TESTING=True,
        REDIS_URL='redis://@localhost:6379/10',
        DRAMATIQ_BROKER='dramatiq.brokers.stub:StubBroker',
        DRAMATIQ_BROKER_URL=None,
        DRAMATIQ_RESULT_BACKEND_URL='redis://@localhost:6379/12',
        STORAGE_CONTAINER=str(uploads_dir),
    ):
        yield


@pytest.fixture
def app(config: None) -> Iterator[Flask]:
    """Obtain configured testing app."""
    from imagestore.app import app, redis
    redis.flushdb()
    with app.app_context():
        yield app


@pytest.fixture
def client(app: Flask):
    """Get test Flask client."""
    return app.test_client()


@pytest.fixture
def upload_file() -> Callable[[], IO]:
    """Get asset file to upload."""
    def get_file() -> IO:
        return (PROJECT_ROOT / 'pull.png').open('rb')

    return get_file


@pytest.fixture
def stub_broker(config):
    """Run test Dramatiq broker."""
    from imagestore.app import flask_dramatiq
    flask_dramatiq.broker.flush_all()
    return flask_dramatiq.broker


@pytest.fixture
def stub_worker(config):
    """Run test Dramatiq worker."""
    from imagestore.app import flask_dramatiq
    from dramatiq import Worker  # type: ignore
    worker = Worker(flask_dramatiq.broker, worker_threads=1, worker_timeout=5)
    worker.start()
    yield worker
    worker.stop()
