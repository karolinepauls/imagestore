"""
Testing scenarios.

Using in-memory Flask client and Dramatiq broker requires heavy mocking. Consider rewriting
to use subprocess services.
"""
from unittest.mock import patch, Mock
from flask import url_for
from dramatiq.message import Message  # type: ignore


def test_upload_transcode_scenario(app, client, upload_file, stub_broker, stub_worker):
    """Upload an image, check formats, and request transcoding."""
    upload_resp = client.post(
        url_for('image_upload'),
        content_type='multipart/form-data',
        buffered=True,
        data={'file': upload_file()},
    )
    assert upload_resp.status_code == 200
    url = upload_resp.json['data']['url']
    name = upload_resp.json['data']['name']
    assert url.endswith(name)
    assert url == url_for('image', storage_name=name)

    retrieve_resp = client.get(url)
    assert retrieve_resp.status_code == 302
    image_location = retrieve_resp.headers['Location']

    image_resp = client.get(image_location)
    assert image_resp.status_code == 200
    assert image_resp.content_type == 'image/png'

    with upload_file() as f:
        assert f.read() == image_resp.data

    formats_resp = client.get(
        url_for('formats', storage_name=name),
    )
    assert formats_resp.status_code == 200
    assert formats_resp.json['data']['extensions'] == ['png']
    assert formats_resp.json['data']['original_extension'] == 'png'

    with patch('imagestore.tasks.requests.get', return_value=Mock(raw=upload_file())) as m:
        transcode_resp = client.post(url_for('transcode', storage_name=name, formats='gif'))
        assert transcode_resp.json['data']['to_transcode'] == ['gif']
        assert transcode_resp.status_code == 200
        stub_broker.join('default')
        stub_worker.join()
        assert m.called

    new_formats_resp = client.get(
        url_for('formats', storage_name=name),
    )
    assert new_formats_resp.status_code == 200
    assert (new_formats_resp.json['data']['original_extension'] ==
            formats_resp.json['data']['original_extension'])
    assert new_formats_resp.json['data']['extensions'] == ['gif', 'png']


def test_upload_transcode_on_demand_scenario(app, client, upload_file, stub_broker, stub_worker):
    """Upload an image and request transcoding by downloading it with a different extension."""
    upload_resp = client.post(
        url_for('image_upload'),
        content_type='multipart/form-data',
        buffered=True,
        data={'file': upload_file()},
    )
    assert upload_resp.status_code == 200
    name = upload_resp.json['data']['name']
    url = upload_resp.json['data']['url']

    orig_get_result = Message.get_result

    def get_result(self, block: bool = False):
        assert block is True
        stub_broker.join('default')
        stub_worker.join()
        return orig_get_result(self, block=block)

    gif_url = url.replace('png', 'gif')
    with patch.object(Message, 'get_result', get_result), \
            patch('imagestore.tasks.requests.get', return_value=Mock(raw=upload_file())):
        retrieve_resp = client.get(gif_url)
    assert retrieve_resp.status_code == 302
    image_location = retrieve_resp.headers['Location']

    image_resp = client.get(image_location)
    assert image_resp.status_code == 200
    assert image_resp.content_type == 'image/gif'

    formats_resp = client.get(
        url_for('formats', storage_name=name),
    )
    assert formats_resp.json['data']['extensions'] == ['gif', 'png']
