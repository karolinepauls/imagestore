#!/usr/bin/env python3
"""Imagestore project setup."""
from pathlib import Path
from setuptools import setup  # type: ignore


VERSION = '0.0.1'
README_FILE = Path(__file__).resolve().with_name('README.rst')
README = README_FILE.read_text('utf-8')
REQUIREMENTS = [
    'Click==7.0',
    'Flask==1.0.2',
    'Flask-Cloudy==1.0.1',
    'flask-redis==0.3.0',
    'flask-dramatiq==0.3.2',
    'pillow==5.4.1',
    'requests==2.21.0',
    'flask-selfdoc==1.1.0',
]
DEV_REQUIREMENTS = [
    'pytest',
    'flake8',
    'pyflakes>=1.6.0',  # Flake8's version doesn't detect types used in string annotations.
    'flake8-docstrings',
    'flake8_tuple',
    'mypy',
    'IPython',
    'pdbpp',
]

if __name__ == '__main__':
    setup(
        name='imagestore',
        version=VERSION,
        description="Image store",
        long_description=README,
        classifiers=[
            'Programming Language :: Python :: 3.6',
            'Programming Language :: Python :: 3.7',
        ],
        author='Karoline Pauls',
        author_email='code@karolinepauls.com',
        license='MIT',
        packages=['imagestore'],
        install_requires=REQUIREMENTS,
        extras_require={
            'dev': DEV_REQUIREMENTS,
        },
    )
