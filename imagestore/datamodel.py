"""Helper functions to query Redis."""
import re
from typing import Tuple, Optional, Set, Union, Iterable
from flask_cloudy import Object   # type: ignore
from werkzeug.datastructures import FileStorage
from imagestore.app import storage, redis
from imagestore.exceptions import APIError


VALID_ID = re.compile('^[0-9a-f]+$')
ACCEPTED_FORMATS = {'bmp', 'png', 'gif'}

extensions_key = 'ext_{}'.format
original_extension_key = 'original_ext_{}'.format


def illegal_formats_error() -> APIError:
    """Build an illegal format exception."""
    return APIError('Illegal formats requested', 400, accepted_formats=sorted(ACCEPTED_FORMATS))


def parse_extensions(request_args: dict) -> Set[str]:
    """Parse and validate formats list."""
    requested_formats = {f for f in request_args.get("formats", '').split(',') if f}
    illegal_formats = requested_formats - ACCEPTED_FORMATS
    if illegal_formats:
        raise illegal_formats_error()
    return requested_formats


def validate_storage_name(storage_name: str) -> Tuple[str, Optional[str]]:
    """
    Extract and validate uuid and extension (if any) from a storage name.

    :raise APIError:
    """
    ext: Optional[str]
    try:
        uuid, ext = storage_name.split('.')
    except ValueError:
        uuid = storage_name
        ext = None

    if not VALID_ID.match(uuid):
        raise APIError('Illegal name', 400)
    if ext is not None and ext not in ACCEPTED_FORMATS:
        raise illegal_formats_error()
    if not image_exists(uuid):
        raise APIError('Image not found', 404)

    return uuid, ext


def storage_upload(file: Union[str, FileStorage], name: Optional[str] = None) -> Object:
    """
    Upload to cloud storage or local storage for development.

    :param name: hex UUID4 and extension or None, in which case the name will be generated.
    """
    uploaded = storage.upload(
        file,
        extensions=ACCEPTED_FORMATS,
        random_name=name is None,
        overwrite=True,
        public=True,
    )
    return uploaded


def image_exists(uuid: str) -> bool:
    """Check if the image exists."""
    return bool(redis.exists(original_extension_key(uuid)))


def get_extensions(storage_name) -> Set[str]:
    """Get already existing extensions."""
    uuid, _ = validate_storage_name(storage_name)
    key = extensions_key(uuid)
    return {f.decode() for f in redis.sscan_iter(key)}


def get_original_extension(uuid: str) -> Optional[str]:
    """Get extension as image's original."""
    extension = redis.get(original_extension_key(uuid))
    if extension is None:
        return None
    return extension.decode()


def register_original_extension(uuid: str, extension: str):
    """Reggister extension as image's original."""
    key = original_extension_key(uuid)
    redis.set(key, extension)
    register_extensions(uuid, [extension])


def register_extensions(uuid: str, extensions: Iterable[str]):
    """Register extension as transcoded."""
    key = extensions_key(uuid)
    redis.sadd(key, *extensions)
