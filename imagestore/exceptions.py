"""API error classes."""


class APIError(Exception):
    """Simple API Error."""

    message: str
    code: int
    data: dict

    def __init__(self, message: str, code: int, **data):
        """Store failure details."""
        self.message = message
        self.code = code
        self.data = data
        super().__init__()
