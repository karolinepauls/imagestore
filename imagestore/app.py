"""Imagestore web API."""
import logging
from flask import Flask
from flask_redis import FlaskRedis  # type: ignore
from flask_cloudy import Storage  # type: ignore
from flask_selfdoc import Autodoc  # type: ignore
from dramatiq.broker import set_broker  # type: ignore
from dramatiq.results.backends import RedisBackend  # type: ignore
from dramatiq.results import Results  # type: ignore
from imagestore.config import Config
from flask_dramatiq import Dramatiq  # type: ignore


logging.basicConfig(level=Config.LOG_LEVEL)
log = logging.getLogger(__name__)
APP_NAME = 'imagestore'
app = Flask(APP_NAME)
auto = Autodoc(app)
app.config.from_object(Config)  # type: ignore

redis = FlaskRedis(app)
redis.init_app(app)

flask_dramatiq = Dramatiq(app)
result_backend = RedisBackend(url=Config.DRAMATIQ_RESULT_BACKEND_URL)
flask_dramatiq.broker.add_middleware(Results(backend=result_backend))
set_broker(flask_dramatiq.broker)
flask_dramatiq.broker.emit_after("process_boot")

storage = Storage()
storage.init_app(app)


# Register views.
import imagestore.views  # noqa: F401
