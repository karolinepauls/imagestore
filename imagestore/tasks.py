"""Imagestore background tasks."""
import logging
import warnings
from io import BytesIO
import requests
from PIL import Image  # type: ignore
from werkzeug.datastructures import FileStorage
from imagestore.app import app, flask_dramatiq as dramatiq, storage
from imagestore.datamodel import register_extensions, get_original_extension, storage_upload


warnings.simplefilter('error', Image.DecompressionBombWarning)
log = logging.getLogger(__name__)


@dramatiq.actor(store_results=True)
def transcode(storage_id: str, target_ext: str):
    """
    Transcode the original version of the image and store.

    We're trying to do as few in-memory copies as possible.
    """
    with app.app_context():
        log.info('Transcoding %r to %r', storage_id, target_ext)
        original_ext = get_original_extension(storage_id)
        original_storage_name = f'{storage_id}.{original_ext}'
        image = storage.get(original_storage_name)
        assert image is not None, f'Image {original_storage_name} must exist'
        resp = requests.get(image.full_url, stream=True)
        image = Image.open(resp.raw)

        target_name = f'{storage_id}.{target_ext}'
        with BytesIO() as transcoded:
            image.save(transcoded, format=target_ext)
            transcoded.seek(0)
            fd = FileStorage(stream=transcoded, filename=target_name)
            obj = storage_upload(fd, name=target_name)
        register_extensions(storage_id, [target_ext])
        log.info('uploaded: %s', obj.name)
        return obj.full_url
