"""Imagestore upload views."""
import logging
from typing import cast
from flask import request, redirect, url_for
from flask_cloudy import InvalidExtensionError  # type: ignore
from werkzeug.datastructures import FileStorage
from imagestore.app import app, auto, storage
from imagestore.datamodel import (
    parse_extensions, validate_storage_name, storage_upload,
    get_original_extension, get_extensions, register_original_extension,
)
from imagestore import tasks
from imagestore.exceptions import APIError
from imagestore.views.api_response import api_response


log = logging.getLogger(__name__)


@app.route("/image", methods=["POST"])
@auto.doc()
def image_upload():
    """
    Handle file upload.

    Upload the image as `file` form data entry.
    Request background transcoding in url param, e.g. `formats=bmp,gif`.
    """
    fd = request.files.get("file")
    if not isinstance(fd, FileStorage):
        raise APIError('Non-file parameter sent as file', 400)
    formats = parse_extensions(request.args)
    try:
        uploaded = storage_upload(fd)
    except InvalidExtensionError:
        return api_response('Invalid file extension', 400)
    else:
        storage_name = cast(str, uploaded.name)
        uuid, ext = storage_name.split('.')
        register_original_extension(uuid, ext)

        for format_ in formats:
            if format_ != ext:
                tasks.transcode.send(uuid, format_)

        return api_response(
            'Uploaded', 200,
            name=storage_name,
            url=url_for('image', storage_name=storage_name, _external=True),
        )


@app.route("/image/<storage_name>", methods=["GET"])
@auto.doc()
def image(storage_name: str):
    """Redirect to the image in the file storage backend, potentially converting prior to that."""
    storage_id, ext = validate_storage_name(storage_name)
    image = storage.get(storage_name)
    if image is not None:
        full_url = image.full_url
    else:
        # Transcode on demand.
        message = tasks.transcode.send(storage_id, ext)
        log.info('Requested transcoding of %r to %r', storage_name, ext)
        full_url = message.get_result(block=True)
    return redirect(full_url)


@app.route("/image/<storage_name>/formats", methods=["GET"])
@auto.doc()
def formats(storage_name: str):
    """Get transcoded formats."""
    storage_id, _ = validate_storage_name(storage_name)
    extensions = get_extensions(storage_id)
    original = get_original_extension(storage_id)
    return api_response(
        'Formats found', 200,
        extensions=sorted(extensions),
        original_extension=original,
    )


@app.route("/image/<storage_name>/transcode", methods=["POST"])
@auto.doc()
def transcode(storage_name: str):
    """
    Request transcoding to another format.

    URL params: format=bmp,gif
    """
    storage_id, _ = validate_storage_name(storage_name)
    extensions = get_extensions(storage_id)
    formats = parse_extensions(request.args)
    to_transcode = formats - extensions
    already_transcoded = formats & extensions

    for ext in to_transcode:
        tasks.transcode.send(storage_id, ext)

    return api_response(
        'Scheduled', 200,
        to_transcode=sorted(to_transcode),
        already_transcoded=sorted(already_transcoded),
    )
