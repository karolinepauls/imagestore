"""Views registry."""
from imagestore.views import errorhandler, uploads, api_response, doc


__all__ = ['errorhandler', 'uploads', 'api_response', 'doc']
