"""Documentation resource."""
from imagestore.app import app, auto


@app.route('/')
def documentation():
    """Render API docs."""
    return auto.html()
