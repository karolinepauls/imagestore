"""Uniform API response builder."""
from flask import jsonify, Response


API_VERSION = 0


def api_response(
    message: str,
    code: int,
    **data,
) -> Response:
    """Build an API response."""
    resp = jsonify({
        'result': 'error' if code >= 400 else 'ok',
        'message': message,
        'code': code,
        'api_version': API_VERSION,
        'data': data,
    })
    resp.status_code = code
    return resp
