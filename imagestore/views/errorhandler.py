"""API error handler."""
from imagestore.app import app
from imagestore.views.api_response import api_response
from imagestore.exceptions import APIError


@app.errorhandler(APIError)
def handle_api_error(e: APIError):
    """Build an error response."""
    return api_response(e.message, e.code, **e.data)
