"""Config from environment."""
from os import environ
from pathlib import Path
import logging


log = logging.getLogger(__name__)

LOCAL = 'LOCAL'
DEV_UPLOADS_FOLDER = Path(__file__).parent.parent / 'uploads'


class Config():
    """Config namespace."""

    SERVER_NAME = '127.0.0.1:5000'
    TESTING = False

    LOG_LEVEL = environ.get('LOG_LEVEL', 'INFO')

    # Image metadata store.
    REDIS_URL = environ.get('REDIS_URL', 'redis://@localhost:6379/0')

    DRAMATIQ_BROKER = environ.get('DRAMATIQ_BROKER', 'dramatiq.brokers.redis:RedisBroker')
    DRAMATIQ_BROKER_URL = environ.get('DRAMATIQ_BROKER_URL', 'redis://@localhost:6379/1')
    DRAMATIQ_RESULT_BACKEND_URL = environ.get('DRAMATIQ_RESULT_BACKEND_URL',
                                              'redis://@localhost:6379/2')

    STORAGE_PROVIDER = environ.get('STORAGE_PROVIDER', LOCAL)
    STORAGE_KEY = environ.get('STORAGE_KEY', None)
    STORAGE_SECRET = environ.get('STORAGE_SECRET', '')
    STORAGE_CONTAINER = environ.get('STORAGE_CONTAINER', DEV_UPLOADS_FOLDER)

    # Will have effect only if using LOCAL as the STORAGE_PROVIDER.
    STORAGE_SERVER_URL = '/uploads/'


assert Config.REDIS_URL != Config.DRAMATIQ_BROKER_URL, (
    'Broker must be distinct from metadata redis.')

# Assert that we don't leak local paths to storage providers.
if Config.STORAGE_PROVIDER.lower() != LOCAL.lower():
    assert Config.STORAGE_CONTAINER != DEV_UPLOADS_FOLDER, "STORAGE_CONTAINER must be configured"
