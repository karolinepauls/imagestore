imagestore
==========

Fetch, store, and process image assets.

Run app (tested on Python 3.7):

.. code:: sh

	pip install -U pip
	pip install .
	FLASK_APP=imagestore.app flask run
	FLASK_APP=imagestore.app flask worker

.. code:: sh

	$ curl 127.0.0.1:5000/image?formats=gif -F 'file=@pull.png'  # Also requesting background conversion to gif.
	{
	  "api_version": 0,
	  "code": 200,
	  "data": {
		"name": "489370ea875c48e6898598b7f7efcd9f.png",
		"url": "http://127.0.0.1:5000/image/489370ea875c48e6898598b7f7efcd9f.png"
	  },
	  "message": "Uploaded",
	  "result": "ok"
	}
	$ curl http://127.0.0.1:5000/image/489370ea875c48e6898598b7f7efcd9f.png/formats  # Already transcoded formats.
	{
	  "api_version": 0,
	  "code": 200,
	  "data": {
		"extensions": [
		  "gif",
		  "png"
		],
		"original_extension": "png"
	  },
	  "message": "Formats found",
	  "result": "ok"
	}
	$ curl http://127.0.0.1:5000/image/489370ea875c48e6898598b7f7efcd9f.png/transcode?formats=png,gif -XPOST  # Will not transcode if already done.
	{
	  "api_version": 0,
	  "code": 200,
	  "data": {
		"already_transcoded": [
		  "gif",
		  "png"
		],
		"to_transcode": []
	  },
	  "message": "Scheduled",
	  "result": "ok"
	}
	$ curl http://127.0.0.1:5000/image/489370ea875c48e6898598b7f7efcd9f.gif  # Redirects to the image store. Will transcode demand (blocking, not recommended).
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
	<title>Redirecting...</title>
	<h1>Redirecting...</h1>
	<p>You should be redirected automatically to target URL: <a href="http://127.0.0.1:5000/uploads/489370ea875c48e6898598b7f7efcd9f.gif">

System requirements
-------------------

- Python 3.7+


Development
-----------

.. code:: sh

	pip install -U pip
	pip install -e .[dev]
	export FLASK_DEBUG=true
	FLASK_APP=imagestore.app flask run
	FLASK_APP=imagestore.app flask worker

.. code:: sh

    py.test -vv --showlocals -s --pdb
